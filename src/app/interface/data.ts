export interface TabInterface {
  name: string;
  selector: string;
}

export interface ReturnTabInterface {
  default_tab: string;
  tabs: TabInterface[];
}

export interface TabSelectorInterface {
  identifier: string;
  selector: any;
  default_tab: string;
}


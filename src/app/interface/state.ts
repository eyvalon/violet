export interface StateInterface {
  name: string;
  value: boolean;
}

export interface DefaultStateInterface {
  identifier: string;
  selector: any;
  default_value: boolean;
}

export interface PageStateInterface {
  identifier: string;
  display: string;
  value: boolean;
}

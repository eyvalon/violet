export interface SearchStatusInterfaces {
  identifier: string;
  status: string;
}

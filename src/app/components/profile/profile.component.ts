import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    user: object;
    isUser: boolean = false;

    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit() {
        this.authService.getProfile().subscribe((profile: any) => {
            this.user = profile.user;
            this.isUser = true;
        },
        err => {
            this.user = null;
            this.isUser = false;
        });
    }

}

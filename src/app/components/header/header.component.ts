import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { StateService } from "../../services/state.service";
import { AuthService } from "../../services/auth.service";
import { faSearch, faBars, faTimes, faUserCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
    // public headerFixedState: boolean;
    public appSearchState: boolean;
    public appMenuState: boolean;

    user: null;
    isUser = false;

    constructor(private router: Router, private stateService: StateService, private authService: AuthService) {}

    ngOnInit() {
        // this.stateService.headerFixedState.subscribe(headerFixedValue => this.headerFixedState = headerFixedValue);
        // this.stateService.appSearchState.subscribe(headerSearchValue => this.appSearchState = headerSearchValue);
        this.stateService.appSearchState.subscribe(appSearchValue => (this.appSearchState = appSearchValue));
        this.stateService.navigationMenuState.subscribe(
            navigationMenuValue => (this.appMenuState = navigationMenuValue)
        );

        if(this.authService.getToken()){
            this.authService.getProfile().subscribe(
                (profile: any) => {
                    this.user = profile.user;
                    this.isUser = true;
                }
            );
        }
    }
    collapse:string = "closed";
    faSearch = faSearch;
    faBars = faBars;
    faUserCircle = faUserCircle;
    faTimes = faTimes;

    toggleCollapse() {
        this.collapse = this.collapse == "open" ? 'closed' : 'open';
    }

    private updateState(target: string): void {
        let currentTargetState: boolean;

        currentTargetState = this.stateService.getState(target);
        currentTargetState == true
            ? this.stateService.stateChange(target, false)
            : this.stateService.stateChange(target, true);
        this.toggleCollapse()
    }

    public toggleTarget(target: string): void {
        switch (target) {
            case "menu": {
                this.updateState("navigation-menu");
                let state: boolean = this.stateService.getState("app-search");
                if (state === true) {
                    this.updateState("app-search");
                }
                break;
            }
            case "search": {
                this.updateState("app-search");

                let state: boolean = this.stateService.getState("app-search");
                state === true
                    ? this.stateService.stateChange("app-scroll", false)
                    : this.stateService.stateChange("app-scroll", true);
                break;
            }
        }
    }

    // indepthSearchListing(): void {
    //   this.router.navigate(['/search']);
    // }

    // appSearchTrigger(): void {
    //   this.stateService.stateChange("app-search-input", true);
    //   this.stateService.stateChange("app-search", true);
    //   // this.stateService.stateChange("header-fixed", true);
    // }
}

import { Component, OnInit, OnDestroy, Renderer2, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';

import { AuthService } from '../../services/auth.service';
import { StateService } from './../../services/state.service';

declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  current_login = false;
  currentUrl;
  // menuState: boolean;
  public navigationMenuState: boolean;
  public PageState: string;

  constructor (
    public authService: AuthService,
    private stateService: StateService,
    private renderer: Renderer2,
    private router: Router) {

    this.currentUrl = "/";
  }

  ngOnDestroy() {
  }

  ngOnInit() {
    this.stateService.navigationMenuState.subscribe(navigationMenuValue => this.navigationMenuState = navigationMenuValue);
    this.stateService.pageState.subscribe(pageValue => this.PageState = pageValue);
    this.current_login = this.authService.isLoggedIn();

    if (!this.current_login) {
      this.authService.logout();
    }
  }

  @HostListener('document:keyup.escape', ['$event'])
  closeNavigationMenu() {
    if (this.stateService.getState("navigation-menu") == true) {
      this.toggleNavigationMenuOff();
    }
  }

  toggleNavigationMenu(): void {
    this.stateService.stateChange("navigation-menu", "auto");

    let currentState = this.stateService.getState("navigation-menu");
    if (currentState == true) {
      this.disableBodyScroll();
    }

    if (currentState == false) {
      this.enableBodyScroll();
    }
  }

  toggleNavigationMenuOff(): void {
    this.stateService.stateChange("navigation-menu", false);
    this.enableBodyScroll();
  }

  private disableBodyScroll(): void {
    this.renderer.addClass(document.body, 'no-scroll');
  }

  private enableBodyScroll(): void {
    this.renderer.removeClass(document.body, 'no-scroll');
  }

  onVideo() {
    this.currentUrl = "/videos";
    this.router.navigateByUrl(this.currentUrl );
  }

  onLogoutClick() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}

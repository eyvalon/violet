import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input } from "@angular/core";
import { makeStateKey, TransferState } from "@angular/platform-browser";
import { Meta, Title } from "@angular/platform-browser";
import { NgProgress } from "@ngx-progressbar/core";

import { DataService } from "../../services/data.service";
import { TabInterface, ReturnTabInterface } from "./../../interface/data";
import { StateService } from "./../../services/state.service";
import { DatabaseService } from "./../../services/database.service";

import { OrderPipe } from "ngx-order-pipe";
import { Observable } from "rxjs";

const DOGS_KEY = makeStateKey("dogs");
const SERIES_KEY = makeStateKey("series");
const MOVIES_KEY = makeStateKey("movies");
const OVA_KEY = makeStateKey("ovas");
const ONAS_KEY = makeStateKey("onas");

@Component({
    selector: "app-anime",
    templateUrl: "./anime.component.html",
    styleUrls: ["./anime.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class AnimeComponent implements OnInit, AfterViewInit {
    dogs: any[];
    series: any[];
    movies: any[];
    ovas: any[];
    onas: any[];
    tabb = [];

    // Lazy loading
    // defaultImage = "https://cdn.myanimelist.net/images/anime/9/64225.jpg";
    // errorImage = "https://cdn.myanimelist.net/images/anime/9/64225.jpg";
    // defaultImage = "";
    errorImage = "assets/images/imageNotFound.jpg";

    // users: any[] = [{}];
    userFilter: any = { title: "" };
    searchTerm: any;

    sIndex: number = 0;
    currentState = false;
    @Input() currentName: object;

    scrollEvent = Observable.fromEvent(document.getElementsByClassName('app-wrapper'), 'scroll')
    page: number = 1;

    isValid: boolean = false;
    issValid: boolean = false;

    // holds data for page & array of tabs
    // MenuInformation: MenuInformationInterface;
    // Store curent selected Tab
    // MenuListSelection: string;

    public tabList: TabInterface[];
    private tabSelected: string;

    constructor(
        private state: TransferState,
        private title: Title,
        private meta: Meta,
        public ngProgress: NgProgress,
        private data: DataService,
        // private menuService: NavigationService,
        private stateService: StateService,
        // private headerService: HeaderService,
        private DB: DatabaseService,
        private orderPipe: OrderPipe
    ) {
        this.stateService.stateChange("default", []);
        this.stateService.pageStateChange("anime");
        let Tab: ReturnTabInterface = this.data.getTabList("anime");
        this.tabSelected = Tab.default_tab;
        this.tabList = Tab.tabs;
    }

    generateContent(count: number): Array<number> {
        const indexes = [];
        for (let i = 0; i < count; i++) {
            indexes.push(i);
        }
        return indexes;
    }

    menuNames: String[];
    // contentNames: String[];

    private tabSelector(tab: string): void {
        if (this.tabSelected != tab) {
            this.tabSelected = tab;
        }
    }

    setState() {
        this.state.set(SERIES_KEY, this.series);
        this.state.set(DOGS_KEY, this.dogs);
        this.state.set(MOVIES_KEY, this.movies);
        this.state.set(OVA_KEY, this.ovas);
        this.currentState = true;
    }

    getState() {
        this.dogs = this.state.get(DOGS_KEY, null as any);
        this.series = this.state.get(SERIES_KEY, null as any);
        this.movies = this.state.get(MOVIES_KEY, null as any);
        this.ovas = this.state.get(OVA_KEY, null as any);
        this.onas = this.state.get(ONAS_KEY, null as any);
    }

    pageChanged(event) {
        this.page = event;
    }

    setIndex(index: number) {
        if (this.data.getChanged()) {
            this.page = this.data.getPage();
            this.sIndex = this.data.getTab();
            this.data.changeChanged();
        } else if (!this.data.getChanged()) {
            this.sIndex = index;
            this.page = 1;
        }

        this.currentName = this.state.get(this.tabb[this.sIndex], null as any);
        // this.setListSelection(this.headerService.getMenuName("menuName")[this.sIndex])

        if (this.currentName === null) {
            this.issValid = false;
        } else {
            this.issValid = true;
        }

        if (!this.currentState) {
            setTimeout(() => {
                // this.DB.initialize();
                this.dogs = this.DB.getD();
                this.series = this.dogs.filter(x => x.type == "TV");
                this.movies = this.dogs.filter(x => x.type == "Movie");
                this.ovas = this.dogs.filter(x => x.type == "OVA");
            }, 1000);
        }

        if (!this.dogs) {
            setTimeout(() => {
                if (!this.currentState) {
                    this.setState();
                } else if (this.currentState) {
                    this.getState();
                }

                this.tabb = [DOGS_KEY, SERIES_KEY, MOVIES_KEY, OVA_KEY, ONAS_KEY];
                this.currentName = this.dogs;

                if (this.currentName === null) {
                    this.issValid = false;
                } else {
                    this.issValid = true;
                }
            }, 2000);
        }
    }

    newMessage(event, item) {
        this.data.changeMessage(this.dogs.find(x => x.mal_id == item));
        this.data.changePage(this.page, this.sIndex);
    }

    initialize() {
        this.currentState = this.DB.init;

        if (!this.currentState) {
            this.issValid = false;
            this.DB.initialize();
        }

        if (this.currentState) {
            this.getState();
        }

        this.tabb = [DOGS_KEY, SERIES_KEY, MOVIES_KEY, OVA_KEY, ONAS_KEY];
        this.currentName = this.dogs;
    }

    ngAfterViewInit() {
        // this.menuService.resetMenuState();
        // if (this.headerService.getMenuState() != "anime") {
        //   this.headerService.updateInformation("initial", "anime", []);
        // }
    }

    ngOnInit() {
        this.title.setTitle("animeX | ANIME");
        this.meta.updateTag({ description: "You can find your anime here" });

        this.initialize();
        setTimeout(() => {
            this.setIndex(this.sIndex);
        }, 1000);

        // this.headerService.updateInformation("initial", "anime", []);
        // this.headerService.updateSelection("anime", "all");
        // this.headerService.information.subscribe(
        //   menuInformation => (this.MenuInformation = menuInformation)
        // );
        // this.headerService.selectedList.subscribe(
        //   menuListSelection => (this.MenuListSelection = menuListSelection)
        // );

        // var element1 = (document.getElementById("contentContainer").style.width =
        //   "");

        // this.menuNames = this.headerService.getMenuName("menuName"); // <- change this varible to use this one instead
        this.menuNames = this.data.test(); //ix this ( redundant stuff)
    }
}

import { Component, OnInit } from "@angular/core";
import { ValidateService } from "../../services/validate.service";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { NgProgress } from "@ngx-progressbar/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  errSwitch = false;
  errMsg: string;
  newUser = false;

  succSwitch = false;
  submitSwitch = false;
  succMsg: string;

  isButtonVisible = false;

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    public ngProgress: NgProgress,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    // this.ngProgress.start();

    if(this.authService.getToken() != null){
      this.router.navigateByUrl("/profile");
    }

    this.registerForm = this.formBuilder.group({
      name: ["", [Validators.required, Validators.minLength(4)]],
      username: ["", [Validators.required, Validators.minLength(6)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  onLoginSubmit(form) {
    const user = {
      username: form.value.username.toLowerCase(),
      password: form.value.password
    };

    this.authService.authenticateUser(user).subscribe((data: any) => {
      if (data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.ngProgress.complete();
        window.location.reload();
      } else {
        this.errSwitch = true;
        this.errMsg = "Invalid username/password";
      }
      // this.ngProgress.complete();
    });

    this.ngProgress.complete();
  }

  onLogin() {}

  get f() { return this.registerForm.controls; }
  onRegister() {
    this.submitted = true;
    this.errMsg = "";
    if (!this.validateService.validateEmail(this.registerForm.value.email)) {
      this.errSwitch = true;
      this.errMsg = "Please use a valid email";
      return false;
    }

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    // Register User
    this.authService.registerUser(this.registerForm.value).subscribe((data: any) => {
      if (data.success) {
        this.succSwitch = true;
        this.succMsg = "You are now registered and can log in";
        this.errSwitch = false;
        this.errMsg = "";
        this.submitSwitch = true;
      } else {
        this.succSwitch = false;
        this.succMsg = "";
        this.errSwitch = true;
        this.errMsg =
          "This username has been taken! Please try a different one.";
      }
    });
  }

  toggleSwitch() {
    this.newUser = !this.newUser;
    this.errMsg = "";
  }
}

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Router } from "@angular/router";

type statField = "title" | "synopsis" | "mal_id";
type StatError = { [u in statField]: string };

interface Ep {
  link: string | null;
  detail: string;
  number: number;
}

@Component({
  selector: "app-create-page",
  templateUrl: "./create-page.component.html",
  styleUrls: ["./create-page.component.scss", "./buttons.css"]
})
export class CreatePageComponent implements OnInit {
  p: number = 1;
  isValid: boolean = false;
  preventAbuse = new Subject<boolean>();

  statForm: FormGroup;

  formError: StatError = {
    title: "",
    synopsis: "",
    mal_id: ""
  };

  validationMessages = {
    title: {
      required: "This field is required.",
      minlength: "Can't be null"
    },
    synopsis: {
      required: "This field is required.",
      minlength: "Can't be null"
    },
    mal_id: {
      required: "This field is required.",
      minlength: "Can't be null"
    }
  };

  defaultImage = "https://myanimelist.cdn-dena.com/images/anime/1057/93850.jpg";
  errorImage = "https://myanimelist.cdn-dena.com/images/anime/1057/93850.jpg";

  public info: any;

  public synopsis;
  public image_url;
  public type;
  public title;
  public title_english;
  public title_japanese;
  public airing;
  public mal_id;
  public duration;
  public score;
  public episodes;
  public premiered;
  public status;

  dataList: any = [];

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm1();
  }

  saveDB() {
    this.http.post("http://localhost:3000/animelist", this.dataList).subscribe(
      res => {
        // let id = res['_id'];
        // console.log(id);
        // this.router.navigate(['/book-details', id]);
      },
      err => {
        console.log(err);
      }
    );
  }

  getStat() {
    this.preventAbuse.next(true);

    var element;
    this.http
      .get<any[]>("https://api.jikan.moe/v3/anime/" + this.mal_id)
      .subscribe(
        data => {
          this.dataList = data;
          this.info = data;
          setTimeout(() => {
            this.synopsis = this.info.synopsis;
            this.image_url = this.info.image_url;
            this.type = this.info.type;
            this.title = this.info.title;
            this.title_english = this.info.title_english;
            this.title_japanese = this.info.title_japanese;
            this.airing = this.info.airing;
            this.duration = this.info.duration;
            this.episodes = this.info.episodes;
            this.score = this.info.score;
            this.premiered = this.info.premiered;
            this.status = this.info.status;

            this.isValid = true;
            this.preventAbuse.next(false);
            // element = document.getElementById('Synopsis').style.height =
            //     '230px';
          }, 1000);

          return;
        },
        err => {
          // element = document.getElementById('Synopsis').style.height = '';
          this.synopsis = null;
          this.image_url = null;
          this.type = null;
          this.type = null;
          this.title = null;
          this.title_english = null;
          this.title_japanese = null;
          this.airing = null;
          this.duration = null;
          this.episodes = null;
          this.score = null;
          this.premiered = null;
          this.status = null;

          this.isValid = false;
          this.preventAbuse.next(false);

          console.error(err);
          return err;
        }
      );
  }

  submit() {
    this.saveDB();
  }

  buildForm1() {
    this.statForm = this.fb.group({
      title: ["", [Validators.required, Validators.minLength(5)]],
      synopsis: ["", [Validators.required, Validators.minLength(15)]],
      mal_id: ["", [Validators.required, Validators.minLength(1)]],
      title_english: ["", []],
      title_japanese: ["", []],
      airing: ["", []],
      duration: ["", []],
      episodes: ["", []],
      premiered: ["", []],
      status: ["", []],
      score: ["", []],
      type: ["", []]
    });

    this.statForm.valueChanges.subscribe(data => this.onValueChanged1(data));
    this.onValueChanged1(); // reset validation messages
  }

  onValueChanged1(data?: any) {
    if (!this.statForm) {
      return;
    }
    const form = this.statForm;
    for (const field in this.formError) {
      if (
        Object.prototype.hasOwnProperty.call(this.formError, field) &&
        field === "title"
      ) {
        // clear previous error message (if any)
        this.formError[field] = "";
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
                this.formError[field] += `${
                  (messages as { [key: string]: string })[key]
                } `;
              }
            }
          }
        }
      }
    }
  }
}

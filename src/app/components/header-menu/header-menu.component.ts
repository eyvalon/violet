import { Component, OnInit, HostListener, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit, AfterViewInit {
    @ViewChild('headerMenuSticky') headerMenu: ElementRef;

    public headerMenuState: boolean;
    private headerMenuPosition: any;
    private headerMenuHeight: any;

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.headerMenu.subscribe(headerStateValue => this.headerMenuState = headerStateValue);
  }

  ngAfterViewInit() {
    this.headerMenuPosition = this.headerMenu.nativeElement.offsetTop;
    this.headerMenuHeight = this.headerMenu.nativeElement.offsetHeight;
  }

  @HostListener('window:scroll', ['$event'])
  headerMenuScroll() {
    const windowScrollPosition = window.pageYOffset;

    if (windowScrollPosition >= (this.headerMenuPosition + this.headerMenuHeight)) {
      // this.headerMenuState = true;
      this.headerService.updateHeaderState(true);
    }else {
      this.headerService.updateHeaderState(false);
      // this.headerMenuState = false;
    }
  }
}

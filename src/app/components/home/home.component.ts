import { Component, OnInit } from "@angular/core";
import { StateService } from '../../services/state.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  ngOnInit() {}

  constructor(private stateService: StateService) {
    this.stateService.stateChange("default", []);
  }
}

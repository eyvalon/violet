import {
  Component,
  Renderer2,
  ElementRef,
  OnInit,
  ViewChild,
  HostListener,
} from "@angular/core";
import { Router } from '@angular/router';
import { StateService } from "../../services/state.service";
import { StateInterface } from "../../interface/state";

import { Meta, Title } from "@angular/platform-browser";
import { NgProgress } from "@ngx-progressbar/core";
import { PipeTransform } from "@angular/core";

import { DataService } from "../../services/data.service";
import { DatabaseService } from "../../services/database.service";
import { SearchStatusInterfaces } from "./../../interface/search";

// import "rxjs/Rx";
import { Observable } from "rxjs";

const stateChanges: StateInterface[] = [
  { name: "header-fixed", value: true },
  { name: "header-search", value: false },
  { name: "footer", value: false }
];

const SearchResetDelay = 5000;
const SearchFocusDelay = 700;

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit, PipeTransform {
  public userFilter: any = { title: "" };
  public searchTerm: any;

  public isValid: boolean = false;
  public issValid: boolean = true;

  private resetSearchInput: any;
  private searchInputFocusState: boolean;

  public appSearchState: boolean;
  public focusSearch: boolean;
  public appSearchOverflowState: boolean;

  public searchInput = "";
  public searchStatus = "Results are shown below. Press ESC to cancel.";

  @ViewChild("UserSearchInput") private userSearchInput: ElementRef;
  private searchStatuses: SearchStatusInterfaces[] = [
    {
      identifier: "default",
      status: "Results are shown below. Press ESC to close search."
    },
    { identifier: "result", status: "Showing results for: " }
  ];

  @ViewChild("UserSearchInput") UserSearchInput: ElementRef;
  public search: string;

  submit(text) {
    this.onSearchChange(text);
    this.userFilter.title = text;
  }

  constructor(
    private title: Title,
    private meta: Meta,
    private stateService: StateService,
    private renderer: Renderer2,
    public ngProgress: NgProgress,
    private data: DataService,
    public DB: DatabaseService,
    private router: Router
  ) {
    this.focusSearch = true;
    this.appSearchOverflowState = false;
  }

  public navigatePage(URL: string){
    this.router.navigate(['/anime/', URL]);
  }

  // Search
  public transform(items: any, term: any): any {
    if (term === undefined) {
      return items;
    }

    let searchedItem = items.filter(item =>
      Object.keys(item).some(
        k =>
          item[k] != null &&
          item[k]
            .toString()
            .toLowerCase()
            .includes(term.toLowerCase())
      )
    );
    return searchedItem;
  }

  public onSearchChange(searchValue: string) {
    if (this.DB.getD()) {
      if (searchValue.length > 0) {
        this.searchTerm = this.transform(this.DB.getD(), searchValue);
        this.isValid = true;
      } else {
        this.searchTerm = null;
        this.isValid = false;
      }
    }
  }

  public initialize() {
    if (!this.DB.init) {
      this.issValid = false;
      this.DB.initialize();
    }

    if (this.DB.getD() === null) {
      this.setStateName();
    } else {
      if (this.data.searchPhrase != "") {
        setTimeout(() => {
          this.userFilter.title = this.data.searchPhrase;
          this.UserSearchInput.nativeElement.value = this.userFilter.title;
          this.isValid = true;
          this.searchTerm = this.transform(
            this.DB.getD(),
            this.userFilter.title
          );
          this.data.searchPhrase = "";
        }, 1000);
      }
    }
  }

  public async setStateName() {
    await setTimeout(() => {
      if (this.data.searchPhrase != "") {
        setTimeout(() => {
          this.userFilter.title = this.data.searchPhrase;
          this.UserSearchInput.nativeElement.value = this.userFilter.title;
          this.isValid = true;
          this.searchTerm = this.transform(
            this.DB.getD(),
            this.userFilter.title
          );
          this.data.searchPhrase = "";
        }, 1000);
      }
    }, 2000);
  }

  @HostListener("document:keyup.enter", ["$event"])
  private searchInputBlur() {
    if (this.searchInputFocusState == true && this.searchInput.length > 0) {
      this.renderer
        .selectRootElement(this.userSearchInput["nativeElement"])
        .blur();
    }
  }

  @HostListener("document:keyup.esc", ["$event"])
  private searchExit() {
    if (this.appSearchState == true) {
      this.searchClear("");
    }
  }

  ngOnInit() {
    Observable.fromEvent(this.UserSearchInput.nativeElement, "keyup")
      .map((evt: any) => evt.target.value)
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe((text: string) => this.submit(text));

    this.title.setTitle("Search Page");
    const metaItems = [
      { name: "description", content: "Get your #1 anime resources!" },
      { property: "og:title", content: "AnimeX | Search" },
      { itemprop: "name", content: "Search Page" },
      { itemprop: "description", content: "Search for existing anime" }
    ];

    this.initialize();

    this.stateService.appSearchState.subscribe(appSearchValue => {
      this.appSearchState = appSearchValue;
      if (this.appSearchState == true) {
        clearTimeout(this.resetSearchInput);
      }

      if (this.appSearchState == true) {
        setTimeout(() => {
          this.searchInputFocus();
        }, SearchFocusDelay);
      } else {
        this.resetSearchInput = setTimeout(() => {
          // this.resetDefault();
          this.updateSearchPhrase("");
        }, SearchResetDelay);
      }
    });
  }

  public toggleSearchInputFocus(): void {
    this.searchInputFocusState = true;
  }

  public toggleSearchInputBlur(): void {
    this.searchInputFocusState = false;
  }

  private getStatusIndex(request: string): number {
    return this.searchStatuses
      .map(status => status.identifier)
      .indexOf(request);
  }

  private updateSearchPhrase(input: string) {
    if (this.DB.getD()) {
      if (input.length > 0) {
        this.searchTerm = this.transform(this.DB.getD(), input);
        this.isValid = true;
        this.focusSearch = false;
        this.appSearchOverflowState = true;
        this.searchStatus =
          this.searchStatuses[this.getStatusIndex("result")].status +
          '"' +
          input +
          '"';
      } else {
        this.searchTerm = null;
        this.isValid = false;
        this.resetDefault();
      }
    }
  }

  public updateSearchInput(input: string): void {
    this.searchInput = input;
    this.updateSearchPhrase(this.searchInput);
  }

  private searchInputFocus(): void {
    this.toggleSearchInputFocus();
    // this.renderer.selectRootElement(this.userSearchInput['nativeElement']).focus();
    this.renderer.selectRootElement(this.userSearchInput.nativeElement).focus();
  }

  public searchClear(input: string): void {
    if (this.searchInput.length > 0 && input === "check") {
      // this.resetDefault();
      this.updateSearchPhrase("");
      // console.log("sds");
    } else {
      this.stateService.stateChange("app-scroll", true);
      this.stateService.stateChange("app-search", false);
    }
  }

  private resetDefault() {
    this.searchInput = "";
    this.focusSearch = true;
    this.appSearchOverflowState = false;
    this.searchStatus = this.searchStatuses[
      this.getStatusIndex("default")
    ].status;
  }

  private resultSelection(event) {
    event.target.className += ' result-selection';
  }

  private resultUnSelection(event) {
    event.target.className = event.target.className.replace(' result-selection', '');
  }
}
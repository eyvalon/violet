import { Component, Inject, OnInit, Renderer2, Input } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { NgProgress } from "@ngx-progressbar/core";
import { DataService } from "../../services/data.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Router } from "@angular/router";
import { DatabaseService } from "./../../services/database.service";
import { StateService } from "./../../services/state.service";

import axios from "axios";
declare var jQuery: any;

@Component({
  selector: "app-tv",
  templateUrl: "./tv.component.html",
  styleUrls: ["./tv.component.scss"]
})
export class TvComponent implements OnInit {
  constructor(
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    public ngProgress: NgProgress,
    private data: DataService,
    private route: ActivatedRoute,
    private DB: DatabaseService,
    private stateService: StateService,
    private router: Router
  ) {
    this.stateService.stateChange("default", []);
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  errorImage = "https://i.imgur.com/XkU4Ajf.png";
  defaultImage = "https://www.placecage.com/1000/1000";

  cover: any;
  image_array: any;
  temp: any;

  images = [
    "assets/images/test.jpg",
    "https://images.unsplash.com/photo-1488388373205-a134c1cc7e4e?dpr=2&auto=compress,format&fit=crop&w=1199&h=799&q=80",
    "https://images.unsplash.com/photo-1422257986712-4f02edc298ce?dpr=2&auto=compress,format&fit=crop&w=1199&h=1199&q=80",
    "https://puu.sh/zK7th/0724b60678.png",
    "https://puu.sh/zK7i1/bf09421bf9.png"
  ];

  extra = true;
  extrasCheckbox = true;

  synopsis = false;
  synopsisCheckbox = false;

  totalEpisodes: any;

  toggleExtras(e) {
    this.extra = e.target.checked;
  }

  toggleSynopsis(e) {
    this.synopsis = e.target.checked;
  }

  message: any;

  init() {
    if (!this.cover) {
      // this.cover = "https://vignette.wikia.nocookie.net/aokana/images/f/fc/Aokana-visual.png/revision/latest/scale-to-width-down/670?cb=20151223124215";
      this.cover =
        "https://f000.backblazeb2.com/file/Anime-List/DZcYj_GU8AAgPPY.jpg";

      (function ($) {
        $(
          '<script src="assets/javascripts/svgInitializer.js" type="text/javascript"></script>'
        ).prependTo("#content");
      })(jQuery);

      startSvg(this.cover);

      // axios.get(`http://localhost:3000/cloudinary/hanasaku_iroha`).then(res => {

      //   this.image_array = res;
      //   this.cover = this.image_array.url;

      //   (function($) {
      //     $(
      //       '<script src="assets/javascripts/svgInitializer.js" type="text/javascript"></script>'
      //     ).prependTo("#content");
      //   })(jQuery);

      //   startSvg("https://vignette.wikia.nocookie.net/aokana/images/f/fc/Aokana-visual.png/revision/latest/scale-to-width-down/670?cb=20151223124215");

      // });
    }
  }

  // Change route if user has not enter with mal_id at the end of the URL / Invalid mal ID not found in DB
  async checkError(pageId) {
    await this.getData(pageId);
    if (!this.message) {
      this.router.navigate(["./404"]);
    }
  }

  async getEpisode(count: number) {
    const indexes = [];
    for (let i = 0; i < count; i++) {
      indexes.push(i);
    }
    this.totalEpisodes = indexes;
  }

  async getData(pageId) {
    let message = "";

    this.message = await axios
      .get("api/animelist/" + pageId)
      .then(function (response) {
        message = response.data.data;
      })
      .catch(function (error) {
        return error;
      })
      .then(function () {
        return message;
      });
  }

  async ngOnInit() {
    this.data.currentMessage.subscribe((message) => (this.message = message));
    let pageId = 0;
    this.route.params.subscribe((params) => {
      pageId = params["id"];
    });

    this.checkError(pageId);

    // Fallback for when user navigate to the page without activating /anime first -> allowing the previous page to load
    // if (this.DB.getD() === undefined || null) {
    //   await this.DB.initialize();
    // }
  }

  startedClass = false;
  completedClass = false;
  preventAbuse = false;

  onStarted() {
    this.startedClass = true;
    setTimeout(() => {
      this.startedClass = false;
    }, 800);
  }

  onCompleted() {
    this.completedClass = true;
    setTimeout(() => {
      this.completedClass = false;
    }, 800);
  }
}

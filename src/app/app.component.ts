import { Component, OnInit } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { StateService } from './services/state.service';

// import Vibrant = require('node-vibrant')

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [
    trigger("popOverState", [
      state(
        "show",
        style({
          opacity: 1,
          transform: "rotate(-135deg)",
          'margin-bottom': '-2px'
          // backgroundColor: 'yellow'
        })
      ),
      state(
        "hide",
        style({
          opacity: 1,
          transform: "rotate(45deg)",
          'margin-bottom': '2px'
          // backgroundColor: 'green'
        })
      ),
      transition("show => hide", animate("250ms ease-out")),
      transition("hide => show", animate("250ms ease-in"))
    ])
  ]
})
export class AppComponent implements OnInit {
  // getColour(url: string){
  //   let v = new Vibrant(url)
  //   v.getPalette((err, palette) => console.log(palette))
  // }

  show: boolean = false;
  clicked = false;

  public appScrollState: boolean;
  public appSearchState: boolean;
  public navigationMenuState: boolean;
  public footerState: boolean;

  constructor (
    private stateService: StateService
  ) {

  //   this.getColour("https://myanimelist.cdn-dena.com/images/anime/1465/93186.jpg");
  //   this.getColour("https://s3.anilist.co/media/anime/cover/large/nx19815-bIo51RMWWhLv.jpg");

  }

  get stateName() {
    return this.show ? "show" : "hide";
  }

  ngOnInit() {
    this.stateService.AppScrollState.subscribe(appScrollValue => this.appScrollState = appScrollValue);
    this.stateService.appSearchState.subscribe(headerSearchValue => this.appSearchState = headerSearchValue);
    this.stateService.navigationMenuState.subscribe(navigationMenuValue => this.navigationMenuState = navigationMenuValue);
    this.stateService.footerState.subscribe(footerStateValue => this.footerState = footerStateValue);
  }

  toggle() {
    this.show = !this.show;
  }


  onClickMe() {
    this.show = !this.show;

    var element1;
    element1 = document.getElementsByClassName("submenu");
    if (element1[0].style.display == "") {
      element1[0].style.display = "list-item";
    } else {
      element1[0].style.display = "";
    }

    // var element2;
    // element2 = document.getElementsByClassName("tc-image-caption1");
    //   element2[0].style.backgroundColor = "yellow";
  }

}

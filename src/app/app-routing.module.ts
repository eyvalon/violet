import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AnimeComponent } from "./components/anime/anime.component";
// import { ChatChannelComponent } from "./components/chat-channel/chat-channel.component";
import { CreatePageComponent } from "./components/create-page/create-page.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { MovieComponent } from "./components/movie/movie.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { SearchComponent } from "./components/search/search.component";
import { TvComponent } from "./components/tv/tv.component";

import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
    { path: "", component: HomeComponent, data: { breadcrumb: "Home" } },
    { path: "login", component: LoginComponent },
    { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
    {
        path: "anime",
        children: [
            {
                path: "series",
                component: TvComponent
            },
            {
                path: ":id",
                component: TvComponent
            },
            { path: "movie", component: MovieComponent },
            { path: "", component: AnimeComponent }
        ]
    },

    {
        path: "create",
        component: CreatePageComponent,
        canActivate: [AuthGuard]
    },

    // { path: "chat/:name", component: ChatChannelComponent },
    { path: "search", component: SearchComponent },
    { path: "**", component: NotFoundComponent }
];

@NgModule({ imports: [RouterModule.forRoot(routes)], exports: [RouterModule] })
export class AppRoutingModule {}

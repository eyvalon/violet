import "prismjs/prism";
import "prismjs/components/prism-typescript";

import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  BrowserModule,
  BrowserTransferStateModule
} from "@angular/platform-browser";
// import {RouterModule, Routes} from '@angular/router';
import { NgProgressModule } from "@ngx-progressbar/core";
import { NgProgressHttpModule } from "@ngx-progressbar/http";
// import {NgProgressRouterModule} from '@ngx-progressbar/router';
import { NgxPaginationModule } from "ngx-pagination";
// import { BreadcrumbModule } from "angular-crumbs";
import { LazyLoadImageModule } from "ng-lazyload-image";
// import { FileUploadModule } from "ng2-file-upload";
// import { NgxUploaderModule } from "ngx-uploader";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FilterPipeModule } from 'ngx-filter-pipe';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AnimeComponent } from "./components/anime/anime.component";
// import { ChatChannelComponent } from "./components/chat-channel/chat-channel.component";
// import { ChatComponent } from "./components/chat/chat.component";
import { CreatePageComponent } from "./components/create-page/create-page.component";
// import { FileUploadComponent } from "./components/file-upload/file-upload.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HeaderComponent } from "./components/header/header.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { MovieComponent } from "./components/movie/movie.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { TvComponent } from "./components/tv/tv.component";
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
  
import { AuthGuard } from "./guards/auth.guard";
import { AuthService } from "./services/auth.service";
// import { ChatService } from "./services/chat.service";
import { ValidateService } from "./services/validate.service";
import { DataService } from "./services/data.service";
import { DatabaseService } from "./services/database.service";

import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { MenuComponent } from './components/menu/menu.component';
import { NavigationService } from './services/navigation.service';
import { HeaderService } from './services/header.service';

// import { FileDropModule } from 'ngx-file-drop';
import { FilterPipe  } from './filter.pipe';
import { OrderModule } from 'ngx-order-pipe';
import { StateService } from './services/state.service';
import { SearchComponent } from './components/search/search.component';

// const config: SocketIoConfig = { url: 'http://localhost:3008', options: {path: '/chats'} };

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    NotFoundComponent,
    AnimeComponent,
    FooterComponent,
    HeaderComponent,
    MovieComponent,
    TvComponent,
    CreatePageComponent,
    // ChatComponent,
    // ChatChannelComponent,
    // FileUploadComponent,
    // BreadcrumbComponent,
    HeaderMenuComponent,
    MenuComponent,
    FilterPipe,
    SearchComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    BrowserTransferStateModule,
    // NgxUploaderModule,
    // FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // RouterModule.forRoot(appRoutes),
    // SocketIoModule.forRoot(config),
    NgProgressModule.forRoot({
      min: 8,
      max: 100,
      ease: "linear",
      speed: 300,
      trickleSpeed: 300,
      meteor: true,
      spinner: false,
      spinnerPosition: "right",
      direction: "ltr+",
      color: "#F643D2",
      thick: false
    }),
    // NgProgressRouterModule,
    NgProgressHttpModule,
    LazyLoadImageModule,
    AppRoutingModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    FilterPipeModule,
    OrderModule,
    FontAwesomeModule,
    // FileDropModule,
    environment.production ? ServiceWorkerModule.register('ngsw-worker.js') : []
  ],
  providers: [
    // ChatService,
    ValidateService,
    AuthService,
    AuthGuard,
    DataService,
    NavigationService,
    HeaderService,
    DatabaseService,
    StateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {}
}

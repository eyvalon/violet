// import { BehaviorSubject } from "rxjs/BehaviorSubject";
// import { Injectable } from "@angular/core";
// import { MenuInformationInterface } from "../interface/header-menu";

// @Injectable()
// export class HeaderService {
//   AnimeTabularList: Array<{ menuName: string; contentName: string }> = [
//     { menuName: "all", contentName: "all-content" },
//     { menuName: "series", contentName: "series-content" },
//     { menuName: "ova", contentName: "ova-content" },
//     { menuName: "movie", contentName: "movie-content" },
//     { menuName: "ona", contentName: "ona-content" },
//     { menuName: "database", contentName: "database-content" },
//     { menuName: "search", contentName: "search-content" }
//   ];

//   private HeaderMenuState: boolean;
//   private MenuListSelection: string = "";
//   private MenuInformation: MenuInformationInterface = {
//     menu: "",
//     menuList: []
//   };

//   private headerMenuState = new BehaviorSubject<boolean>(false);
//   private listState = new BehaviorSubject<string>(this.MenuListSelection);
//   private menuState = new BehaviorSubject<MenuInformationInterface>(this.MenuInformation);

//   public readonly headerMenu = this.headerMenuState.asObservable();
//   public readonly selectedList = this.listState.asObservable();
//   public readonly information = this.menuState.asObservable();

//   public updateHeaderState(requestState: boolean): void {
//     let currentState = this.headerMenuState.getValue();
//     if (requestState != currentState) {
//       if (currentState == false) {
//         this.headerMenuState.next(true);
//       }else {
//         this.headerMenuState.next(false);
//       }
//     }
//   }

//   getMenuName(type: String) {
//     let results = [];
//     for (var x in this.AnimeTabularList) {
//       if (type == "menuName") results.push(this.AnimeTabularList[x].menuName);
//       else if (type == "contentName") results.push(this.AnimeTabularList[x].contentName);
//     }
//     return results;
//   }


//   constructor() {}

//   public updateInformation(
//     type: string,
//     origin: string,
//     MenuList: string[]
//   ): void {
//     this.MenuInformation.menu = origin;
//     if (type == "") {
//       this.MenuInformation.menuList = MenuList;
//     } else {
//       if (type == "initial" && origin == "anime") {
//         this.MenuInformation.menuList = this.getMenuName("menuName");
//       }
//     }
//     this.menuState.next(this.MenuInformation);
//   }

//   public resetInformation(): void {
//     this.MenuInformation.menu = "";
//     this.MenuInformation.menuList = [];
//     this.menuState.next(this.MenuInformation);
//   }

//   public updateSelection(origin: string, selection: string): void {
//     this.MenuListSelection = selection;
//     this.listState.next(this.MenuListSelection);
//   }

//   public resetSelection(): void {}

//   public getMenuState(): string {
//     return this.MenuInformation.menu;
//   }
// }

import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Injectable } from "@angular/core";

@Injectable()
export class HeaderService {

  private HeaderMenuState: boolean;

  private headerMenuState = new BehaviorSubject<boolean>(false);

  public readonly headerMenu = this.headerMenuState.asObservable();

  public updateHeaderState(requestState: boolean): void {
    let currentState = this.headerMenuState.getValue();
    if (requestState != currentState) {
      if (currentState == false) {
        this.headerMenuState.next(true);
      }else {
        this.headerMenuState.next(false);
      }
    }
  }


  constructor() {}

}

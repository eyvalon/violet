import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
// import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';

export class ChatService {
    private url = 'http://localhost:3008';
    private socket;

    constructor() {
        //Comment this out and the service worker gets registered fine
        this.socket = io(this.url, {
            path: '/chat'
        });
    }
    
    sendMessage(message){
        this.socket.emit('add-message', message);
    }

    newUser(name){
        this.socket.emit('new-user', name);
    }

    roomInfo(room){
        this.socket.emit('room-info', room);
    }

    getMessages() {
        let observable = new Observable(observer => {
            this.socket.on('message', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;
    }
}

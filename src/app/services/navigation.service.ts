import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class NavigationService {

  private menuState = new BehaviorSubject<boolean>(false);
  public readonly menuTrigger = this.menuState.asObservable();

  constructor() { }

  changeMenuState(): boolean {
    let state: boolean;

    if (this.menuState.getValue() == true) {
      this.menuState.next(false);
      state = false;
    }else if (this.menuState.getValue() == false) {
      this.menuState.next(true);
      state = true;
    }
    return state;
  }

  resetMenuState(): void {
    this.menuState.next(false);
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StateInterface, DefaultStateInterface, PageStateInterface } from '../interface/state';

@Injectable()
export class StateService {
  private AppScroll = new BehaviorSubject<boolean>(true);
  private PageState = new BehaviorSubject<string>("");
  private NavigationMenu = new BehaviorSubject<boolean>(false);
  private HeaderFixed = new BehaviorSubject<boolean>(false);
  // private HeaderMenu = new BehaviorSubject<boolean>(false); // Check to see if is true
  // private HeaderMenuFixed = new BehaviorSubject<boolean>(false);
  private AppSearch = new BehaviorSubject<boolean>(false);
  private AppSearchInput = new BehaviorSubject<boolean>(false);
  private Footer = new BehaviorSubject<boolean>(true);

  public readonly AppScrollState = this.AppScroll.asObservable();
  public readonly pageState = this.PageState.asObservable();
  public readonly navigationMenuState = this.NavigationMenu.asObservable();
  public readonly headerFixedState = this.HeaderFixed.asObservable();
  // public readonly headerMenuState = this.HeaderMenu.asObservable(); // Check to see if is true
  // public readonly headerMenuFixedState = this.HeaderMenuFixed.asObservable();
  public readonly appSearchState = this.AppSearch.asObservable();
  public readonly appSearchInputState = this.AppSearchInput.asObservable();
  public readonly footerState = this.Footer.asObservable();

  private PageStates: PageStateInterface[] = [
    {identifier: "home", display: "home", value: false},
    {identifier: "anime", display: "anime", value: false},
    {identifier: "user", display: "user", value: false}
  ];

  private States: DefaultStateInterface[] = [
    {identifier: "app-scroll", selector: this.AppScroll, default_value: true},
    {identifier: "navigation-menu", selector: this.NavigationMenu, default_value: false},
    {identifier: "header-fixed", selector: this.HeaderFixed, default_value: false},
    // {identifier: "header-menu", selector: this.HeaderMenu, default_value: false}, // Check to see if is true
    // {identifier: "header-menu-fixed", selector: this.HeaderMenuFixed, default_value: false},
    {identifier: "app-search", selector: this.AppSearch, default_value: false},
    {identifier: "app-search-input", selector: this.AppSearchInput, default_value: false},
    {identifier: "footer", selector: this.Footer, default_value: true},
  ];

  constructor() { }

  public getState(identity: string): any {
    let index: number = this.States.map((states) => states.identifier).indexOf(identity);

    if (index != -1) {
      return this.States[index].selector.getValue();
    }

    return "";
  }

  private initStateChange(selector: any, stateRequest: any): void {
    if (selector.getValue() != stateRequest) {
      selector.next(stateRequest);
    }
  }

  private initDefaultPageState(stateRequests: StateInterface[]): void {
    let stateMap = stateRequests.map((states) => states.name);

    for (let i = 0; i < this.States.length; i++) {
      let index: number;
      (stateRequests.length >= 1) ? index = stateMap.indexOf(this.States[i].identifier) : index = -1;

      if (index != -1) {
        this.initStateChange(this.States[i].selector, stateRequests[index].value);
      }else {
        this.initStateChange(this.States[i].selector, this.States[i].default_value);
      }
    }
  }

  public pageStateChange(identity: string): void {
    for (let i = 0; i < this.PageStates.length; i++) {
      if (this.PageStates[i].value == true && this.PageStates[i].identifier != identity) {
        this.PageStates[i].value = false;
      }

      if (this.PageStates[i].identifier == identity && this.PageStates[i].value == false) {
        this.PageStates[i].value = true;
        this.initStateChange(this.PageState, this.PageStates[i].display);
      }
    }
  }

  public stateChange(identity: string, stateRequest: any): void {
    let index: number = this.States.map((states) => states.identifier).indexOf(identity);

    if (identity == "navigation-menu") {
      if (stateRequest != "auto") {
        this.initStateChange(this.States[index].selector, stateRequest);
      }else {
        let currentState = this.getState("navigation-menu");
        if (currentState == true) {
          this.initStateChange(this.States[index].selector, false);
        }else if (currentState == false) {
          this.initStateChange(this.States[index].selector, true);
        }
      }
    }else if (identity == "default") {
      this.initDefaultPageState(stateRequest);
    }else {
      this.initStateChange(this.States[index].selector, stateRequest);
    }
  }
}

import { Injectable } from "@angular/core";
import { NgProgress } from "@ngx-progressbar/core";
import { makeStateKey, TransferState } from "@angular/platform-browser";
import { HttpClient } from "@angular/common/http";

const DOGS_KEY = makeStateKey("dogs");
const SERIES_KEY = makeStateKey("series");
const MOVIES_KEY = makeStateKey("movies");
const OVA_KEY = makeStateKey("ovas");
const ONAS_KEY = makeStateKey("onas");

import axios from "axios";

@Injectable()
export class DatabaseService {
  dogs: any;
  series: any;
  movies: any;
  ovas: any;
  onas: any;

  init = false;

  getD() {
    return this.dogs;
  }

  constructor(
    public ngProgress: NgProgress,
    private state: TransferState,
    private http: HttpClient
  ) {}

  async initialize() {
    this.dogs = this.state.get(DOGS_KEY, null as any);
    this.series = this.state.get(SERIES_KEY, null as any);
    this.movies = this.state.get(MOVIES_KEY, null as any);
    this.ovas = this.state.get(OVA_KEY, null as any);
    this.onas = this.state.get(ONAS_KEY, null as any);

    await this.http.get("api/animelist").subscribe((data) => {
      let newData = data;
      this.dogs = newData;
      this.state.set(DOGS_KEY, newData as any);

      this.series = this.dogs.filter((x) => x.type == "TV");
      this.movies = this.dogs.filter((x) => x.type == "Movie");
      this.ovas = this.dogs.filter((x) => x.type == "OVA");

      this.state.set(SERIES_KEY, this.series);
      this.state.set(MOVIES_KEY, this.movies);
      this.state.set(OVA_KEY, this.ovas);
    });

    this.init = true;
  }
}

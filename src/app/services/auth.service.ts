import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {

    authToken: string;
    @Input() user: any;

    constructor(private http: HttpClient) { }

    registerUser(user) {
        user.username = user.username.toLowerCase();
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post('users/register', user, { headers });
    }

    authenticateUser(user) {
        let headers = new HttpHeaders()
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post('/api/users/authenticate', user, { headers });
    }

    getProfile() {
        this.loadToken();
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', this.authToken);
        return this.http.get('/api/users/profile', { headers });
    }

    storeUserData(token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    }

    loadToken() {
        const token = localStorage.getItem('id_token');
        this.authToken = token;
    }

    getName() {
        let userx = localStorage.getItem('user');
        return JSON.parse(userx);
    }

    getToken() {
        const token = localStorage.getItem('id_token');

        let payload;

        if (token) {
            payload = token.split('.')[1];
            payload = window.atob(payload);
            return JSON.parse(payload);
        } else {
            return null;
        }
    }

    isLoggedIn(){
        this.authToken = this.getToken();

        if (this.authToken) {
            return true;
        } else {
            return false;
        }
    }

    loggedIn() {
        this.isLoggedIn();

        if (this.authToken) {
            return true;
        } else {
            return false;
        }
    }

    logout() {
        this.authToken = "";
        this.user = null;
        localStorage.clear();
    }

}

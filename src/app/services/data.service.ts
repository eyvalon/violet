import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { NgProgress } from "@ngx-progressbar/core";
import { TabInterface, ReturnTabInterface, TabSelectorInterface } from './../interface/data';

@Injectable()
export class DataService {

  private AnimeTabList: TabInterface[] = [
    { name: "all", selector: "anime-all" },
    { name: "series", selector: "anime-series" },
    { name: "ova" , selector: "anime-ova" },
    { name: "movie", selector: "anime-movie" },
    { name: "ona", selector: "anime-ona" },
  ];

  private AnimeIDTabList: TabInterface[] = [
    { name: "information", selector: "anime-information" },
    { name: "episodes", selector: "anime-episodes" }
  ];

  private UserTabList: TabInterface[] = [
    { name: "dashboard", selector: "user-dashboard" }
  ];

  private AdminTabList: TabInterface[] = [
    { name: "databse", selector: "admin-database" },
    // { name: "Databse", selector: "admin-??" },
  ];

  private Tabs: TabSelectorInterface[] = [
    {identifier: "anime", selector: this.AnimeTabList, default_tab: "all"},
    {identifier: "anime-id", selector: this.AnimeIDTabList, default_tab: "information"},
    {identifier: "user", selector: this.UserTabList, default_tab: "dashboard"},
    {identifier: "admin", selector: this.AdminTabList, default_tab: "dashboard"}
  ];

  private messageSource = new BehaviorSubject("default message");
  currentMessage = this.messageSource.asObservable();

  private currentPage: number = 0;
  private currentTab: number = 0;
  private hasChanged:boolean = false;
  public searchPhrase: String = "";

  constructor(public ngProgress: NgProgress) {}

  public getTabList(identity: string): ReturnTabInterface {
    let index: number = this.Tabs.map((tabs) => tabs.identifier).indexOf(identity);
    let details: ReturnTabInterface = {
      default_tab: this.Tabs[index].default_tab,
      tabs: this.Tabs[index].selector
    };
    return details;
  }

  public test() {
    return this.AnimeTabList.map((tabs) => tabs.name);
  }

  public setSearchPhrase(search: any) {
    this.searchPhrase = search;
  }
  
  changeMessage(message: any) {
    this.messageSource.next(message);
  }

  changePage(page: number, tab: number){
    this.currentPage = page;
    this.currentTab = tab;
    this.hasChanged = true;
  }

  getPage(){
    return this.currentPage;
  }

  getTab(){
    return this.currentTab;
  }

  getChanged(){
    return this.hasChanged;
  }

  changeChanged(){
    this.hasChanged = false;
  }
}

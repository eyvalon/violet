$('.navigationLink').on("click",function() {
    $(this).addClass('active').siblings().removeClass('active');
});

$(window).scroll(function() {
    if ($(window).scrollTop() > 20 ) {
        $('.underlay').addClass('display-underlay');
    } else {
        $('.underlay').removeClass('display-underlay');
    };
});

// -------------------------
// INITIALIZER DOCUMENT
// -------------------------
//     File Name: initializer
//     File Src: src/assets/javascript/anime/series
//     URI Section: /anime/series
//
//     Extra Information: SVG creation (D3) and responsive design
//                        Vibrant colour pallete generator
// -------------------------
var debug = true;

var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

var backgroundHeight = 50.4;
var backgroundHeightOffset = 0.1;
var themelight = '#F7F7F7';


var backgroundURL = 'https://res.cloudinary.com/eyvalon/image/upload/v1524312078/DZcYj_GU8AAgPPY.jpg';
var coverURL = 'assets/images/Caligula_Anime.jpg';


// var backgroundURL = 'assets/images/Caligula_Anime.jpg';
// var coverURL = 'assets/images/violet.jpg';

// -------------------------
// IMAGE OBJECT DECLARATIONS
// -------------------------
var background = {
    src: null,
    vibrant: null,
    dark_vibrant: null,
    light_vibrant: null,
    title: null,
    body: null,
    muted: null,
    dark_muted: null
}

var cover = {
    src: null,
    vibrant: null,
    dark_vibrant: null,
    light_vibrant: null,
    title: null,
    body: null,
    muted: null,
    dark_muted: null
}
var default_background = {
    src: 'assets/images/fanart3.jpg',
    vibrant: null,
    dark_vibrant: null,
    light_vibrant: null,
    title: null,
    body: null,
    muted: null,
    dark_muted: null
}

var default_cover = {
    src: 'assets/images/fanart1.jpg',
    vibrant: null,
    dark_vibrant: null,
    light_vibrant: null,
    title: null,
    body: null,
    muted: null,
    dark_muted: null
}

// -------------------------
// IMAGE TO BASE64 ENCODER FUNCTION
// -------------------------
function base64Encoder(src, callback) {
    var image = new Image();
    image.crossOrigin = "Anonymous";

    image.onload = function() {
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var base64_dataURL;

        canvas.height = this.naturalHeight;
        canvas.width = this.naturalWidth;
        context.drawImage(this, 0, 0);
        base64_dataURL = canvas.toDataURL("image/png");
        callback(base64_dataURL);
    };

    image.onerror = function() {
        callback("IMG_ERR");
    }

    image.src = src;
    if (image.complete || (image.complete == undefined)) {
        image.src = 'data:image/jpg;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
        image.src = src;
    }
}

// -------------------------
// GENERATE BASE64 FOR BACKGROUND & COVER
// -------------------------
base64Encoder(backgroundURL, function(base64_dataURL) {
    background.src = base64_dataURL;

    base64Encoder(coverURL, function(base64_dataURL) {
        cover.src = base64_dataURL;

        // -------------------------
        // GENERATE SWATCHES FOR BACKGROUND & COVER
        // -------------------------
        /* Generate and ini colour palette (swatches) matching the specified image
        Generated palettes include:
            Vibrant
            Muted
            DarkVibrant
            DarkMuted
            LightVibrant
            getTitleTextColor()
            getBodyTextColor()
        */
        // -------------------------
        var generatedBackground = document.createElement('img');
        if (background.src != "IMG_ERR") {
            generatedBackground.setAttribute('src', background.src);
            if (debug == true) {
                console.log("SUCCESS: Background image loaded")
            }
        }else {
            generatedBackground.setAttribute('src', default_background.src);
            if (debug == true) {
                console.log("ERROR: IMG_ERR \nReverting back to default background")
            }
        }

        var generatedCover = document.createElement('img');
        if (cover.src != "IMG_ERR") {
            generatedCover.setAttribute('src', cover.src);
            if (debug == true) {
                console.log("SUCCESS: Cover image loaded")
            }
        }else {
            generatedCover.setAttribute('src', default_cover.src);
            if (debug == true) {
                console.log("ERROR: IMG_ERR \nReverting back to default cover")
            }
        }

        generatedBackground.addEventListener('load', function() {
            var generatedBackgroundSwatches = new Vibrant(generatedBackground);
            var backgroundSwatches = generatedBackgroundSwatches.swatches();
            background.vibrant = backgroundSwatches.Vibrant.getHex();
            background.dark_vibrant = backgroundSwatches.DarkVibrant.getHex();
            background.light_vibrant = backgroundSwatches.LightVibrant.getHex();
            background.title = backgroundSwatches.LightVibrant.getTitleTextColor();
            background.body = backgroundSwatches.LightVibrant.getBodyTextColor();
            background.muted = backgroundSwatches.Muted.getHex();
            background.dark_muted = backgroundSwatches.DarkMuted.getHex();

            if (debug == true) {
                console.log("--- BACKGROUND ---");
                console.log('Vibrant: ' + background.vibrant);
                console.log('Muted: ' + background.muted);
                console.log('Dark Vibrant: ' + background.dark_vibrant);
                console.log('Dark Muted: ' + background.dark_muted);
                console.log('Light Vibrant: ' + background.light_vibrant);
                console.log('Title Text: ' + background.title);
                console.log('Body Text: ' + background.body);
                console.log('---------------');
            }

            generatedCover.addEventListener('load', function() {
                var generatedCoverSwatches = new Vibrant(generatedCover);
                var coverSwatches = generatedCoverSwatches.swatches();
                cover.vibrant = coverSwatches.Vibrant.getHex();
                cover.dark_vibrant = coverSwatches.DarkVibrant.getHex();
                cover.light_vibrant = coverSwatches.LightVibrant.getHex();
                cover.title = coverSwatches.LightVibrant.getTitleTextColor();
                cover.body = coverSwatches.LightVibrant.getBodyTextColor();
                cover.muted = coverSwatches.Muted.getHex();
                cover.dark_muted = coverSwatches.DarkMuted.getHex();

                if (debug == true) {
                    console.log("--- COVER ---");
                    console.log('Vibrant: ' + cover.vibrant);
                    console.log('Muted: ' + cover.muted);
                    console.log('Dark Vibrant: ' + cover.dark_vibrant);
                    console.log('Dark Muted: ' + cover.dark_muted);
                    console.log('Light Vibrant: ' + cover.light_vibrant);
                    console.log('Title Text: ' + cover.title);
                    console.log('Body Text: ' + cover.body);
                    console.log('---------------');
                }

                // -------------------------
                // SVG INITIALIZER
                // -------------------------
                var backgroundInitializer = d3.select('#background')
                    .attr('viewBox', '0 0 225 78.5')
                    .attr('preserveAspectRatio', 'none');

                var spacerInitializer = d3.select('#spacer')
                    .attr('preserveAspectRatio', 'none');

                // -------------------------
                // SVG DEFS INITIALIZER
                // -------------------------
                var backgroundDefs = backgroundInitializer.insert("defs",":first-child");

                // Background Image Defs Pattern Initializer
                backgroundDefs.append("pattern")
                    .attr('id', 'img-background')
                    .attr('patternContentUnits', 'objectBoundingBox')
                    .attr('width', '100%')
                    .attr('height', '100%')
                .append("image")
                    .attr('preserveAspectRatio', 'none')
                    .attr('xlink:href', background.src)
                    .attr('width', '1')
                    .attr('height', '1.25');

                // Cover Image Defs Pattern Initializer
                backgroundDefs.append("pattern")
                    .attr('id', 'img-cover')
                    .attr('patternContentUnits', 'objectBoundingBox')
                    .attr('preserveAspectRatio', 'xMidYMid slice')
                    .attr('width', '100%')
                    .attr('height', '100%')
                .append("image")
                    .attr('preserveAspectRatio', 'xMidYMid slice')
                    .attr('xlink:href', cover.src)
                    .attr('width', '1')
                    .attr('height', '1');

                // Background Image Defs Gradient Fade Initializer
                var backgroundGradient = backgroundDefs.append("linearGradient")
                    .attr('id', 'img-backgroundFade')
                    .attr('patternContentUnits', 'objectBoundingBox')
                    .attr('y1', '100%')
                    .attr('x2', '0%');

                backgroundGradient.append("stop")
                    .attr('stop-color', themelight)
                    .attr('stop-opacity', '1')
                    .attr('offset', '0%');

                backgroundGradient.append("stop")
                    .attr('stop-color', themelight)
                    .attr('stop-opacity', '0')
                    .attr('offset', '100%');

                // Upper Cover Holder Line Up Fade Initializer
                var backgroundUpperLineGradient = backgroundDefs.append("linearGradient")
                    .attr('id', 'upperLineUpFade')
                    .attr('patternContentUnits', 'objectBoundingBox')
                    .attr('y1', '100%')
                    .attr('x2', '0%');

                backgroundUpperLineGradient.append("stop")
                    .attr('stop-opacity', '1')
                    .attr('stop-color', cover.light_vibrant)
                    .attr('offset', '0%');

                backgroundUpperLineGradient.append("stop")
                    .attr('stop-opacity', '0')
                    .attr('stop-color', cover.muted)
                    .attr('offset', '100%');

                // Upper Cover Holder Line Down Fade Initializer
                var backgroundLowerLineGradient = backgroundDefs.append("linearGradient")
                    .attr('id', 'upperLineDownFade')
                    .attr('patternContentUnits', 'objectBoundingBox')
                    .attr('x1', '0%')
                    .attr('y2', '100%');

                backgroundLowerLineGradient.append("stop")
                    .attr('stop-opacity', '1')
                    .attr('stop-color', background.light_vibrant)
                    .attr('offset', '0%');

                backgroundLowerLineGradient.append("stop")
                    .attr('stop-opacity', '0')
                    .attr('stop-color', background.muted)
                    .attr('offset', '100%');

                // -------------------------
                // SVG PATH/LINE COLOUR INITIALIZER
                // -------------------------

                // Upper Holder Initializer
                d3.select('#holder-upper')
                    .attr('fill', cover.light_vibrant);

                // Center Holder Initializer
                d3.select('#holder-center')
                    .attr('fill', background.vibrant);

                // Lower Line Initializer - Lower Line (Up)
                d3.select('#line-lower-up')
                    .attr('stroke', cover.muted);

                // Lower Line Initializer - Lower Line (Down)
                d3.select('#line-lower-down')
                    .attr('stroke', background.vibrant);

                // Lower Holder Initializer
                d3.select('#holder-lower')
                    .attr('fill', cover.muted);

                // Cover Holder Initializer
                d3.select('#holder-cover')
                    .attr('fill', background.light_vibrant);

                // Cover with Frame/Accent Initializer
                d3.select('#coverFrame')
                    .attr('stroke', background.title);

                // Cover Image Accent Initializer
                d3.select('#coverAccent')
                    .attr('stroke', cover.vibrant);


/*                 d3.select('#cover').attr('fill', 'url('+location.href+'#img-cover)');
                d3.select('#cover').select('id').remove(); */

                backgroundInitializer.attr('visibility', 'visible');






































            });
        });
    });
});

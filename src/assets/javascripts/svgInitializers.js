
// Svg Initializer
var svgInitializer = d3.select('#contentSpacer').append("svg")
    .attr('id', 'svgcontentSpacer')
    .attr('viewBox', '0 0 225 38')
    .attr('preserveAspectRatio', 'none')
    .attr('visibility', 'hidden');

var animeBackgroundLength = 50;
var animeBackgroundOffset = 0.1;
var themelight = '#F7F7F7';

var backgroundImageWidth = 0.57;
var coverImageWidth = 1;

var widescreenStandard = 16/9;
var normalStandard = 4/3;

function widescreenWidth(width) {
    return width * (widescreenStandard * 2);
}

function normalWidth(width) {
    return width * normalStandard;
}


var animeBackgroundSrc;

if (verge.viewportW() <= 576) {
    animeBackgroundSrc = 'assets/images/small.jpg';
}else {
     animeBackgroundSrc = 'https://res.cloudinary.com/eyvalon/image/upload/v1524312078/DZcYj_GU8AAgPPY.jpg';
     // animeBackgroundSrc = 'assets/images/Ao_no_Kanata_no_Four_Rhythm_Background.jpg';
//var animeBackgroundSrc = 'images/Untitled-1.jpg';
}

//alert(window.verge.viewportW());

//alert(intViewportWidth);

var animeCoverSrc = 'assets/images/Ao_no_Kanata_no_Four_Rhythm_Image.jpg';
var debug = true;

// Preloading Images and Image Callback
function preloadimages(imageList) {
    var image = [];
    var postaction = function(){};
    var imageListLoaded = 0;

    function imageloadpost(){
        imageListLoaded ++;
        if (imageListLoaded == imageList.length) {
            postaction(image);
        }
    }

    for (var i=0; i<imageList.length; i++) {
        image[i] = new Image();
        image[i].src = imageList[i];
image[i].setAttribute('crossOrigin', '');

        image[i].onload = function() {
            if (debug == true) {
                console.log('Image loaded successfully: ' + image[i]);
            }
            imageloadpost();
        };

        image[i].onerror = function() {
            if (debug == true) {
                console.log('Image loaded unsuccessfully: ' + image[i].src);
            }
            imageloadpost();
        };
    }

    return {
        loaded:function(images) {
            postaction = images;
        }
    };
}

preloadimages([animeBackgroundSrc, animeCoverSrc]).loaded(function(images) {
/* Generate and ini colour palette (swatches) matching the specified image
Generated palettes include:
    Vibrant
    Muted
    DarkVibrant
    DarkMuted
    LightVibrant
    getTitleTextColor()
    getBodyTextColor()
*/
    var animeBackgroundColorPalette = new Vibrant(images[0]);
    var backgroundSwatches = animeBackgroundColorPalette.swatches();
    if (debug == true) {
        console.log('Bacground Image: ' + images[0]);
        console.log('Vibrant: ' + backgroundSwatches.Vibrant.getHex());
        console.log('Muted: ' + backgroundSwatches.Muted.getHex());
        console.log('Dark Vibrant: ' + backgroundSwatches.DarkVibrant.getHex());
        console.log('Dark Muted: ' + backgroundSwatches.DarkMuted.getHex());
        console.log('Light Vibrant: ' + backgroundSwatches.LightVibrant.getHex());
        console.log('Title Text: ' + backgroundSwatches.LightVibrant.getTitleTextColor());
        console.log('Body Text: ' + backgroundSwatches.LightVibrant.getBodyTextColor());
        console.log('---------------');
    }

    var animeCoverColorPalette = new Vibrant(images[1]);
    var coverSwatches = animeCoverColorPalette.swatches();
    if (debug == true) {
        console.log('Cover Image: ' + images[1]);
        console.log('Vibrant: ' + coverSwatches.Vibrant.getHex());
        console.log('Muted: ' + coverSwatches.Muted.getHex());
        console.log('Dark Vibrant: ' + coverSwatches.DarkVibrant.getHex());
        console.log('Dark Muted: ' + coverSwatches.DarkMuted.getHex());
        console.log('Light Vibrant: ' + coverSwatches.LightVibrant.getHex());
        console.log('Title Text: ' + coverSwatches.LightVibrant.getTitleTextColor());
        console.log('Body Text: ' + coverSwatches.LightVibrant.getBodyTextColor());
        console.log('---------------');
    }

// Svg Initializer
var svgInitializer = d3.select('#contentBackground').append("svg")
    .attr('id', 'svgContentBackground')
    .attr('viewBox', '0 0 225 78.5')
    .attr('preserveAspectRatio', 'none')
    .attr('visibility', 'hidden');

/*
===========================
DEFS GENERATING STARTS HERE
===========================
*/
// Defs Initializer
var svgDefsInitializer = svgInitializer.append("defs");

// Anime Background Initializer
svgDefsInitializer.append("pattern")
        .attr('id', 'animeBackground')
        .attr('patternContentUnits', 'objectBoundingBox')
        .attr('width', '100%')
        .attr('height', '100%')
    .append("image")
        .attr('preserveAspectRatio', 'none')
        .attr('xlink:href', animeBackgroundSrc)
        .attr('width', backgroundImageWidth)
        .attr('height', widescreenWidth(backgroundImageWidth));

// Anime Cover Initializer
svgDefsInitializer.append("pattern")
        .attr('id', 'animeCover')
        .attr('patternContentUnits', 'objectBoundingBox')
        .attr('width', '100%')
        .attr('height', '100%')
    .append("image")
        .attr('preserveAspectRatio', 'none')
        .attr('xlink:href', animeCoverSrc)
        .attr('width', coverImageWidth)
        .attr('height', normalWidth(coverImageWidth));

// Anime Background Gradient Fade Initializer
var svgBackgroundGradientInitializer = svgDefsInitializer.append("linearGradient")
        .attr('id', 'animeBackgroundFade')
        .attr('patternContentUnits', 'objectBoundingBox')
        .attr('y1', '100%')
        .attr('x2', '0%');

svgBackgroundGradientInitializer.append("stop")
    .attr('stop-color', themelight)
    .attr('stop-opacity', '1')
    .attr('offset', '0%');

svgBackgroundGradientInitializer.append("stop")
    .attr('stop-color', themelight)
    .attr('stop-opacity', '0')
    .attr('offset', '100%');

// Upper Cover Holder Line Up Fade Initializer
var svgUpperLineUpGradientInitializer = svgDefsInitializer.append("linearGradient")
        .attr('id', 'upperLineUpFade')
        .attr('patternContentUnits', 'objectBoundingBox')
        .attr('y1', '100%')
        .attr('x2', '0%');

svgUpperLineUpGradientInitializer.append("stop")
    .attr('stop-opacity', '1')
    .attr('stop-color', coverSwatches.LightVibrant.getHex())
    .attr('offset', '0%');

svgUpperLineUpGradientInitializer.append("stop")
    .attr('stop-opacity', '0')
    .attr('stop-color', coverSwatches.Muted.getHex())
    .attr('offset', '100%');

// Upper Cover Holder Line Down Fade Initializer
var svgUpperLineDownGradientInitializer = svgDefsInitializer.append("linearGradient")
    .attr('id', 'upperLineDownFade')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('x1', '0%')
    .attr('y2', '100%');

svgUpperLineDownGradientInitializer.append("stop")
    .attr('stop-opacity', '1')
    .attr('stop-color', backgroundSwatches.LightVibrant.getHex())
    .attr('offset', '0%');

svgUpperLineDownGradientInitializer.append("stop")
    .attr('stop-opacity', '0')
    .attr('stop-color', backgroundSwatches.Muted.getHex())
    .attr('offset', '100%');

/*
================================
PATH/LINE GENERATING STARTS HERE
================================
*/
// Background Anime Image
svgInitializer.append("path")
    .attr('fill', 'url(' + location.href + '#animeBackground)')
    .attr('d', 'M 0 0 v' + animeBackgroundLength + 'h 400 v -' + animeBackgroundLength + 'Z');

// // Background Anime Image Fade - Initial
// svgInitializer.append("path")
//     .attr('fill', 'url(#animeBackgroundFade)')
//     .attr('d', 'M 0 0 v' + (animeBackgroundLength + animeBackgroundOffset) + 'h 225 v -' + (animeBackgroundLength + animeBackgroundOffset) + 'Z');

// // Background Anime Image Fade - Finisher
// svgInitializer.append("path")
//     .attr('fill', 'url(#animeBackgroundFade)')
//     .attr('d', 'M 0 0 v' + (animeBackgroundLength + animeBackgroundOffset) + 'h 225 v -' + (animeBackgroundLength + animeBackgroundOffset) + 'Z');


// Background Anime Image Fade - Initial
svgInitializer.append("path")
    .attr('fill', 'url(' + location.href + '#animeBackgroundFade)')
    // .attr('fill-opacity', '0.99')
    .attr('d', 'M 0 0 v' + (animeBackgroundLength + animeBackgroundOffset ) + 'h 225 v -' + (animeBackgroundLength + animeBackgroundOffset ) + 'Z');

// Background Anime Image Fade - Finisher
svgInitializer.append("path")
    .attr('fill', 'url(' + location.href + '#animeBackgroundFade)')
    .attr('fill-opacity', '0.48')
    .attr('d', 'M 0 35 v  15 h 225 v -15 Z');


// Upper Cover Holder
svgInitializer.append("path")
    .attr('fill-opacity', '0.2')
    .attr('fill', coverSwatches.LightVibrant.getHex())
    .attr('d', 'M 12.5 37.1 l -15 -15 l 15 -15 l 15 15 Z');

// Upper Cover Holder - Middle Line (Up)
svgInitializer.append("line")
    .attr('stroke', 'url(' + location.href + '#upperLineUpFade)')
    .attr('stroke-width', '0.2')
    .attr('stroke-linecap', 'butt')
    .attr('x1', '12.75')
    .attr('y1', '7.5')
    .attr('x2', '0')
    .attr('y2', '21.1');

// Upper Cover Holder - Middle Line (Down)
svgInitializer.append("line")
    .attr('stroke', 'url(' + location.href + '#upperLineDownFade)')
    .attr('stroke-width', '0.2')
    .attr('stroke-linecap', 'butt')
    .attr('x1', '12.75')
    .attr('y1', '36.75')
    .attr('x2', '0')
    .attr('y2', '23');

// Lower Cover Holder - Middle
svgInitializer.append("path")
    .attr('fill', backgroundSwatches.Vibrant.getHex())
    .attr('d', 'M 0 41 v 7 l 3.5 -3.5 Z');

// Lower Cover Holder - Middle Line (Down)
svgInitializer.append("line")
    .attr('stroke-width', '0.3')
    .attr('stroke-dasharray', '0.2')
    .attr('stroke', coverSwatches.Muted.getHex())
    .attr('x1', '0')
    .attr('y1', '39')
    .attr('x2', '23.1')
    .attr('y2', '62.35');

// Lower Cover Holder - Lower Line (Up)
svgInitializer.append("line")
    // .attr('class', 'lowerLineDashed')
    .attr('stroke-width', '0.3')
    .attr('stroke-dasharray', '0.2')
    .attr('stroke', backgroundSwatches.Vibrant.getHex())
    .attr('x1', '24.05')
    .attr('y1', '54.45')
    .attr('x2', '0')
    .attr('y2', '78.5');

// Lower Cover Holder - Joiner
svgInitializer.append("path")
    .attr('fill-opacity', '0.3')
    .attr('fill', coverSwatches.Muted.getHex())
    .attr('d', 'M 10 45 l -15.25 15.25 l 15.25 15.25 l 15.25 -15.25 Z');

// Lower Cover Holder
svgInitializer.append("path")
    .attr('fill-opacity', '0.6')
    .attr('stroke', themelight)
    .attr('stroke-width', '0.8')
    .attr('fill', backgroundSwatches.LightVibrant.getHex())
    .attr('d', 'M 15.75 46 v 13 h 13 Z');

// Anime Cover with Frame/Accent
svgInitializer.append("path")
    .attr('fill', 'none')
    .attr('stroke-width', '0.8')
    .attr('stroke-linejoin', 'miter')
    .attr('stroke', backgroundSwatches.Vibrant.getTitleTextColor())
    .attr('d', 'M 31.05 18 l -22 22 l 22 22 l 22 -22 Z');

svgInitializer.append("path")
    .attr('fill', 'none')
    .attr('stroke-width', '0.75')
    .attr('stroke-opacity', '0.4')
    .attr('stroke-linejoin', 'bevel')
    .attr('stroke', coverSwatches.Vibrant.getHex())
    .attr('d', 'M 31.05 21 l -19 19 l 19 19 l 19 -19 Z');

// Anime Cover Image
svgInitializer.append("path")
    .attr('fill', 'url(' + location.href + '#animeCover)')
    .attr('d', 'M 31.05 21 l -19 19 l 19 19 l 19 -19 Z');

svgInitializer.attr('visibility', 'visible');
});


if (matchMedia) {
  mq = window.matchMedia("(max-width: 667px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}

// media query change
function WidthChange(mq) {
  if (mq.matches) {
      var test = d3.select('#animeBackground').select('image')
        .attr('xlink:href', 'assets/images/small.jpg');
//
    d3.selectAll("svg")
    // .attr('viewBox', '0 0 54 78.5')

  } else {
      var test = d3.select('#animeBackground').select('image')
        .attr('xlink:href', 'assets/images/Ao_no_Kanata_no_Four_Rhythm_Background.jpg');

      d3.selectAll("svg")
    // .attr('viewBox', '0 0 225 78.5');

  }

}

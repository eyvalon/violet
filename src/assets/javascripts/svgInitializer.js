// -------------------------
// INITIALIZER DOCUMENT
// -------------------------
//     File Name: initializer
//     File Src: src/assets/javascript/anime/series
//     URI Section: /anime/series
//
//     Extra Information: SVG creation (D3) and responsive design
//                        Vibrant colour pallete generator
// -------------------------
var debug = true;

function svgInit(background, cover) {
// -------------------------
// iOS OPERATING SYSTEM CHECK
// -------------------------
    function iOSCheck() {
        return (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform));
    }

// -------------------------
// SVG SELECTOR INITIALIZER
// -------------------------
    var backgroundInitializer = d3.select('#background')
    .attr('preserveAspectRatio', 'none');

    var spacerInitializer = d3.select('#spacer')
    .attr('preserveAspectRatio', 'none');

// -------------------------
// SVG DEFS INITIALIZER
// -------------------------
    d3.select('#img-background-link')
        .attr('xlink:href', background.src)
        .attr('width', '1')
        .attr('height', '1');

    d3.select('#img-cover-link')
        .attr('xlink:href', cover.src)
        .attr('width', '1')
        .attr('height', '1');

    d3.select('#upperLineUpFade-start')
        .attr('stop-color', cover.light_vibrant);

    d3.select('#upperLineUpFade-end')
        .attr('stop-color', cover.muted);

    d3.select('#upperLineDownFade-start')
        .attr('stop-color', background.light_vibrant);

    d3.select('#upperLineDownFade-end')
        .attr('stop-color', background.light_vibrant);

// -------------------------
// SVG PATH/LINE COLOUR INITIALIZER
// -------------------------
    // Operating System: iOS fix for invalid fills (absolute path)
    if (iOSCheck() == true) {
        d3.select('#backgroundWallpaper').attr('fill', 'url('+location.href+'#img-background)');
        d3.select('#backgroundWallpaperPrime').attr('fill', 'url('+location.href+'#img-backgroundFade)');
        d3.select('#backgroundWallpaperFinish').attr('fill', 'url('+location.href+'#img-backgroundFade)');
        d3.select('#line-upper-up').attr('stroke', 'url('+location.href+'#upperLineUpFade)');
        d3.select('#line-upper-down').attr('stroke', 'url('+location.href+'#upperLineDownFade)');
        d3.select('#cover').attr('fill', 'url('+location.href+'#img-cover)');
    }else {
        d3.select('#backgroundWallpaper').attr('fill', 'url(#img-background)');
        d3.select('#backgroundWallpaperPrime').attr('fill', 'url(#img-backgroundFade)');
        d3.select('#backgroundWallpaperFinish').attr('fill', 'url(#img-backgroundFade)');
        d3.select('#line-upper-up').attr('stroke', 'url(#upperLineUpFade)');
        d3.select('#line-upper-down').attr('stroke', 'url(#upperLineDownFade)');
        d3.select('#cover').attr('fill', 'url(#img-cover)');
    }

    // Upper Holder Initializer
    d3.select('#holder-upper')
        .attr('fill', cover.light_vibrant);

    // Center Holder Initializer
    d3.select('#holder-center')
        .attr('fill', background.vibrant);

    // Lower Line Initializer - Lower Line (Up)
    d3.select('#line-lower-up')
        .attr('stroke', cover.muted);

    // Lower Line Initializer - Lower Line (Down)
    d3.select('#line-lower-down')
        .attr('stroke', background.vibrant);

    // Lower Holder Initializer
    d3.select('#holder-lower')
        .attr('fill', cover.muted);

    // Cover Holder Initializer
    d3.select('#holder-cover')
        .attr('fill', background.light_vibrant);

    // Cover with Frame/Accent Initializer
    d3.select('#coverFrame')
        .attr('stroke', background.title);

    // Cover Image Accent Initializer
    d3.select('#coverAccent')
        .attr('stroke', cover.vibrant);

    backgroundInitializer.attr('visibility', 'visible');
    if (debug == true) {
        console.log("SVG done, script done")
    }
}

// -------------------------
// Check image src for a valid image (img)
// base64 Encoder for cover and background (canvas)
// Generate colour pallete for images (imag)
// -------------------------
var load = (function() {
    function _load(tag) {
        return function(src_path, type) {
            return new Promise(function(resolve, reject) {
                var image;

                if ((tag == 'canvas') || (tag == 'exist')) {
                    image = new Image();
                    if (tag == 'canvas') {
                        image.crossOrigin = "Anonymous";
                    }
                }else if (tag == "img") {
                    image = document.createElement(tag);
                }

                image.onload = function() {
                    if (tag == 'canvas') {
                        var canvas = document.createElement(tag);
                        var context = canvas.getContext('2d');

                        canvas.height = this.naturalHeight;
                        canvas.width = this.naturalWidth;
                        context.drawImage(this, 0, 0);

                        resolve(canvas.toDataURL("image/png"));
                    }else if (tag == 'img') {
                        var imageSwatches = {
                            src: null,
                            vibrant: null,
                            dark_vibrant: null,
                            light_vibrant: null,
                            title: null,
                            body: null,
                            muted: null,
                            dark_muted: null
                        }

                        var generateSwatches = new Vibrant(image);
                        var swatches = generateSwatches.swatches();

                        imageSwatches.src = src_path;
                        imageSwatches.vibrant = swatches.Vibrant.getHex();
                        imageSwatches.dark_vibrant = swatches.DarkVibrant.getHex();
                        imageSwatches.light_vibrant = swatches.LightVibrant.getHex();
                        imageSwatches.title = swatches.LightVibrant.getTitleTextColor();
                        imageSwatches.body = swatches.LightVibrant.getBodyTextColor();
                        imageSwatches.muted = swatches.Muted.getHex();
                        imageSwatches.dark_muted = swatches.DarkMuted.getHex();

                        resolve(imageSwatches);
                    }else if (tag == 'exist') {
                        resolve("true");
                    }
                }

                image.onerror = function() {
                    if (tag == 'exist') {
                        var errorDetail = {
                            location: type,
                            background: undefined,
                            cover: undefined
                        }

                        if (type == "background") {
                            errorDetail.background = true;
                        }

                        if (type == "cover") {
                            errorDetail.cover = true;
                        }

                        reject(errorDetail);
                    }
                }
                image.src = src_path;
            });
        };
    };
    return {
        canvas: _load('canvas'),
        img: _load('img'),
        img_exists: _load('exist')
    }
})();

// -------------------------
// function call to Generate colour pallete for images (imag)
// -------------------------
function swatchesInit(background, cover) {
    Promise.all([
        load.img(background, undefined),
        load.img(cover, undefined)
    ]).then(function(image) {
        if (debug == true) {
            console.log(image[0]);
            console.log(image[1]);
            console.log('SUCCESS: All images swatches generated\nProceeding to initailze SVG!');
        }
        svgInit(image[0], image[1]);
    });
}

// -------------------------
// function call to base64 Encoder for cover and background (canvas)
// -------------------------
function base64Encoder(backgroundURL, coverURL) {
    Promise.all([
        load.canvas(backgroundURL, undefined),
        load.canvas(coverURL, undefined)
    ]).then(function(encodedImage) {
        if (debug == true) {
            console.log('SUCCESS: All images encoded\nProceeding to generate swatches!');
        }
        swatchesInit(encodedImage[0], encodedImage[1]);
    });
}

// -------------------------
// Check image src for a valid image (img)
// Start initailing cover and background SVG
// -------------------------
function init(backgroundURL, coverURL) {
    Promise.all([
        load.img_exists(backgroundURL, "background"),
        load.img_exists(coverURL, "cover")
    ]).then(function() {
        if (debug == true) {
            console.log('SUCCESS: All images valid\nProceeding to encode to base 64!');
        }
        base64Encoder(backgroundURL, coverURL);
    }).catch(function(error) {
        if (error.background == true) {
            if (debug == true) {
                console.log(error);
                console.log('ERROR: Invalid images\nFindg invalid images and reverting to default (B)!');
            }
            backgroundURL = default_backgroundURL;
            Promise.all([
                load.img_exists(coverURL, undefined)
            ]).then(function() {
                if (debug == true) {
                    console.log('SUCCESS: All images valid\nProceeding to encode to base 64 (Defaults - B)!\n\nError details:\nCover: OK\nBackground: OK (Reverting Default)');
                    console.log('------ RESOLVED ERROR (B) -------');
                }
                base64Encoder(backgroundURL, coverURL);
            }).catch(function(error) {
                if (debug == true) {
                    console.log('ERROR: Invalid images (All)\nProceeding to encode to base 64 (Defaults - C+B)!\n\nError details:\nCover: OK (Reverting Default)\nBackground: OK (Reverting Default)');
                    console.log('------ RESOLVED ERROR (C/B) -------');
                }
                coverURL = default_coverURL;
                base64Encoder(backgroundURL, coverURL);
            });
        }else if (error.cover == true) {
            if (debug == true) {
                console.log(error);
                console.log('ERROR: Invalid images\nFindg invalid images and reverting to default (C)!');
            }
            coverURL = default_coverURL;
            Promise.all([
                load.img_exists(backgroundURL, undefined)
            ]).then(function() {
                if (debug == true) {
                    console.log('SUCCESS: All images valid\nProceeding to encode to base 64 (Defaults - C)!\n\nError details:\nCover: OK (Reverting Default)\nBackground: OK');
                    console.log('------ RESOLVED ERROR (C) -------');
                }
                base64Encoder(backgroundURL, coverURL);
            }).catch(function(error) {
                if (debug == true) {
                    console.log('ERROR: Invalid images (All)\nProceeding to encode to base 64 (Defaults - C+B)!\n\nError details:\nCover: OK (Reverting Default)\nBackground: OK (Reverting Default)');
                    console.log('------ RESOLVED ERROR (C/B) -------');
                }
                backgroundURL = default_backgroundURL;
                base64Encoder(backgroundURL, coverURL);
            });
        }
    });
}

var default_coverURL = 'assets/images/fanart1.jpg';
var default_backgroundURL = 'assets/images/fanart1.jpg';
var coverURL = 'assets/images/Calisgula_Anime.jpg';

function startSvg(imageURL) {
    // var backgroundURL = 'https://res.cloudinary.com/eyvalon/image/upload/s--MOQrprep--/v1524312078/hanasaku_iroha.jpg';
    init(imageURL, coverURL);
}

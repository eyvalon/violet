var intViewportWidth = window.innerWidth;

//var animeBackgroundSrc;
//alert(verge.viewportW());
//if (intViewportWidth <= 576) {
//    console.log(intViewportWidth);
//    animeBackgroundSrc = 'images/fanart1.jpg';
//}else {
var animeBackgroundSrc = 'assets/images/Ao_no_Kanata_no_Four_Rhythm_Background.jpg';
//}

//alert(window.verge.viewportW());

//alert(intViewportWidth);

var animeCoverSrc = 'assets/images/Ao_no_Kanata_no_Four_Rhythm_Image.jpg';
var debug = true;

// Preloading Images and Image Callback
function preloadimages(imageList) {
    var image = [];
    var postaction = function(){};
    var imageListLoaded = 0;

    function imageloadpost(){
        imageListLoaded ++;
        if (imageListLoaded == imageList.length) {
            postaction(image);
        }
    }

    for (var i=0; i<imageList.length; i++) {
        image[i] = new Image();
        image[i].src = imageList[i];

        image[i].onload = function() {
            if (debug == true) {
                console.log('Image loaded successfully: ' + image[i]);
            }
            imageloadpost();
        };

        image[i].onerror = function() {
            if (debug == true) {
                console.log('Image loaded unsuccessfully: ' + image[i].src);
            }
            imageloadpost();
        };
    }

    return {
        loaded:function(images) {
            postaction = images;
        }
    };
}

preloadimages([animeBackgroundSrc, animeCoverSrc]).loaded(function(images) {
    /* Generate and ini colour palette (swatches) matching the specified image
Generated palettes include:
    Vibrant
    Muted
    DarkVibrant
    DarkMuted
    LightVibrant
    getTitleTextColor()
    getBodyTextColor()
*/
    var animeBackgroundColorPalette = new Vibrant(images[0]);
    var backgroundSwatches = animeBackgroundColorPalette.swatches();
    if (debug == true) {
        console.log('Bacground Image: ' + images[0]);
        console.log('Vibrant: ' + backgroundSwatches.Vibrant.getHex());
        console.log('Muted: ' + backgroundSwatches.Muted.getHex());
        console.log('Dark Vibrant: ' + backgroundSwatches.DarkVibrant.getHex());
        console.log('Dark Muted: ' + backgroundSwatches.DarkMuted.getHex());
        console.log('Light Vibrant: ' + backgroundSwatches.LightVibrant.getHex());
        console.log('Title Text: ' + backgroundSwatches.LightVibrant.getTitleTextColor());
        console.log('Body Text: ' + backgroundSwatches.LightVibrant.getBodyTextColor());
        console.log('---------------');
    }

    var animeCoverColorPalette = new Vibrant(images[1]);
    var coverSwatches = animeCoverColorPalette.swatches();
    if (debug == true) {
        console.log('Cover Image: ' + images[1]);
        console.log('Vibrant: ' + coverSwatches.Vibrant.getHex());
        console.log('Muted: ' + coverSwatches.Muted.getHex());
        console.log('Dark Vibrant: ' + coverSwatches.DarkVibrant.getHex());
        console.log('Dark Muted: ' + coverSwatches.DarkMuted.getHex());
        console.log('Light Vibrant: ' + coverSwatches.LightVibrant.getHex());
        console.log('Title Text: ' + coverSwatches.LightVibrant.getTitleTextColor());
        console.log('Body Text: ' + coverSwatches.LightVibrant.getBodyTextColor());
        console.log('---------------');
    }

    // Svg Initializer
    var svgInitializer = d3.select('#contentBackground').append("svg")
    .attr('viewBox', '0 0 225 98.55')
    .attr('visibility', 'hidden');

    /*
===========================
DEFS GENERATING STARTS HERE
===========================
*/
    // Defs Initializer
    var svgDefsInitializer = svgInitializer.append("defs");

    // Anime Background Initializer
    svgDefsInitializer.append("pattern")
    .attr('id', 'animeBackground')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('width', '100%')
    .attr('height', '100%')
    .append("image")
    .attr('preserveAspectRatio', 'none')
    .attr('xlink:href', animeBackgroundSrc)
    .attr('width', '0.57')
    .attr('height', '1.55');

    // Anime Cover Initializer
    svgDefsInitializer.append("pattern")
    .attr('id', 'animeCover')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('width', '100%')
    .attr('height', '100%')
    .append("image")
    .attr('preserveAspectRatio', 'none')
    .attr('xlink:href', animeCoverSrc)
    .attr('width', '1')
    .attr('height', '1');

    // Anime Background Fade Initializer
    var svgBackgroundLinearGradientInitializer = svgDefsInitializer.append("linearGradient")
    .attr('id', 'animeBackgroundFade')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('y1', '100%')
    .attr('x2', '0%');

    svgBackgroundLinearGradientInitializer.append("stop")
    .attr('id', 'backgroundFadeStart')
    .attr('offset', '0%');

    svgBackgroundLinearGradientInitializer.append("stop")
    .attr('id', 'backgroundFadeEnd')
    .attr('offset', '100%');

    // Upper Cover Holder Line Down Fade Initializer
    var svgupperCoverHolderLineDownLinearGradientInitializer = svgDefsInitializer.append("linearGradient")
    .attr('id', 'upperCoverHolderLineDownFade')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('y1', '100%')
    .attr('x2', '0%');

    svgupperCoverHolderLineDownLinearGradientInitializer.append("stop")
    .attr('class', 'upperCoverHolderLineFadeStart')
    .attr('stop-color', coverSwatches.LightVibrant.getHex())
    .attr('offset', '0%');

    svgupperCoverHolderLineDownLinearGradientInitializer.append("stop")
    .attr('class', 'upperCoverHolderLineFadeEnd')
    .attr('stop-color', coverSwatches.Muted.getHex())
    .attr('offset', '100%');

    // Upper Cover Holder Line Up Fade Initializer
    var svgupperCoverHolderLineUpLinearGradientInitializer = svgDefsInitializer.append("linearGradient")
    .attr('id', 'upperCoverHolderLineUpFade')
    .attr('patternContentUnits', 'objectBoundingBox')
    .attr('x1', '0%')
    .attr('y2', '100%');

    svgupperCoverHolderLineUpLinearGradientInitializer.append("stop")
    .attr('class', 'upperCoverHolderLineFadeStart')
    .attr('stop-color', backgroundSwatches.LightVibrant.getHex())
    .attr('offset', '0%');

    svgupperCoverHolderLineUpLinearGradientInitializer.append("stop")
    .attr('class', 'upperCoverHolderLineFadeEnd')
    .attr('stop-color', backgroundSwatches.Muted.getHex())
    .attr('offset', '100%');

    /*
================================
PATH/LINE GENERATING STARTS HERE
================================
*/
    // Background Anime Image
    svgInitializer.append("path")
    .attr('id', 'background')
    .attr('d', 'M 0 0 v 65 h 400 v -65 Z');

    // Background Anime Image Fade
    svgInitializer.append("path")
    .attr('id', 'backgroundFade')
    .attr('d', 'M 0 0 v 65.5 h 400 v -65.5 Z');

    // Upper Cover Holder
    svgInitializer.append("path")
    .attr('id', 'upperCoverHolder')
    .attr('fill', coverSwatches.LightVibrant.getHex())
    .attr('d', 'M 12.5 40.1 l -15 -15 l 15 -15 l 15 15 Z');

    // Upper Cover Holder - Middle Line (Down)
    svgInitializer.append("line")
    .attr('id', 'upperCoverHolderLineDown')
    .attr('class', 'lineFade')
    .attr('x1', '13.25')
    .attr('y1', '10')
    .attr('x2', '0')
    .attr('y2', '24.25');

    // Upper Cover Holder - Middle Line (Up)
    svgInitializer.append("line")
    .attr('id', 'upperCoverHolderLineUp')
    .attr('class', 'lineFade')
    .attr('x1', '12.75')
    .attr('y1', '39.75')
    .attr('x2', '0')
    .attr('y2', '26');

    // Lower Cover Holder
    svgInitializer.append("path")
    .attr('id', 'lowerCoverHolder')
    .attr('fill', coverSwatches.Muted.getHex())
    .attr('d', 'M 3.5 65 l 14.05 14.05 l 14.05 -14.05 l -14.05 -14.05 Z');

    // Lower Cover Holder - Lower Line
    svgInitializer.append("line")
    .attr('class', 'lineDashed')
    .attr('stroke', backgroundSwatches.Vibrant.getHex())
    .attr('x1', '19.05')
    .attr('y1', '52.45')
    .attr('x2', '0')
    .attr('y2', '71.5');

    // Lower Cover Holder - Middle
    svgInitializer.append("path")
    .attr('fill', backgroundSwatches.Vibrant.getHex())
    .attr('d', 'M 0 61.5 v 7 l 3.5 -3.5 Z');

    // Lower Cover Holder - Middle Line
    svgInitializer.append("line")
    .attr('class', 'lineDashed')
    .attr('stroke', coverSwatches.Muted.getHex())
    .attr('x1', '0')
    .attr('y1', '65')
    .attr('x2', '14')
    .attr('y2', '79');

    // Lower Cover Holder - Lower
    svgInitializer.append("path")
    .attr('fill', backgroundSwatches.Vibrant.getHex())
    .attr('d', 'M 17.55 79.05 h -17.55 v 19.5 Z');

    // Anime Cover with Frame/Accent
    svgInitializer.append("path")
    .attr('id', 'coverFrame')
    .attr('stroke', backgroundSwatches.Vibrant.getTitleTextColor())
    .attr('d', 'M 31.05 21 l -22 22 l 22 22 l 22 -22 Z');

    svgInitializer.append("path")
    .attr('id', 'coverAccent')
    .attr('stroke', coverSwatches.Vibrant.getHex())
    .attr('d', 'M 31.05 24 l -19 19 l 19 19 l 19 -19 Z');

    // Anime Cover Image
    svgInitializer.append("path")
    .attr('id', 'cover')
    .attr('d', 'M 31.05 24 l -19 19 l 19 19 l 19 -19 Z');

    svgInitializer.attr('visibility', 'visible');
});

//if (matchMedia) {
//  const mq = window.matchMedia("(max-width: 576px)");
//  mq.addListener(WidthChange);
//  WidthChange(mq);
//}

//// media query change
//function WidthChange(mq) {
//  if (mq.matches) {
//    console.log(";sdsdsdsds");
//      var test = d3.select('#animeBackground').select('image')
//        .attr('xlink:href', 'images/fanart1.jpg');
//
//  } else {
//      var test = d3.select('#animeBackground').select('image')
//        .attr('xlink:href', 'images/Untitled-1.jpg');
//  }
//
//}

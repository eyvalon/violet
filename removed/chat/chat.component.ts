import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Title } from "@angular/platform-browser";

import { ChatService } from "../../services/chat.service";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
  providers: [ChatService]
})
export class ChatComponent implements OnInit {
  public rForm: FormGroup;
  post: any;
  name;
  message = "";

  nameValidator: string = "New";
  messages = [];
  time = [];
  connection;
  roomId;
  isUserMessage = false;

  signedInUser: false;
  user: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private chatService: ChatService,
    public auth: AuthService,
    private fb: FormBuilder,
    private title: Title,
  ) {
    this.rForm = fb.group({
      message: [
        "",
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(100)
        ]
      ],
      validate: ""
    });
  }

  checkName(name, message) {
    return this.signedInUser;
  }

  sendMessage() {
    if (this.auth.loggedIn()) {
      this.chatService.sendMessage(this.message);
      this.message = "";
    } else {
      this.message = "Log in to chat";
    }
  }

  newUser() {
    this.chatService.newUser(this.name);
  }

  newGuest() {
    this.chatService.newUser("Guest");
  }

  registerRoom() {
    this.chatService.roomInfo(this.roomId);
  }

  ngOnInit() {
    // Get's the current logged in user to display as the username
    this.user = this.auth.getName();
    this.name = this.user.name;

    // Get's the channel's parameter for unique guests
    this.activatedRoute.params.subscribe((params: Params) => {
      this.roomId = params["name"];
    });

    this.title.setTitle("Welcome to " + this.roomId + " channel");

    if (!this.auth.loggedIn()) {
      this.message = "Log in to chat";
    }

    this.chatService.getMessages().subscribe(message => {
      const nd = new Date();
      let currentData = message;
      console.log(currentData);

      const data = {
        message,
        time: nd.getHours() + ":" + nd.getMinutes(),
        isNewUser: currentData["type"],
        isUser: currentData["text"].includes(this.name)
      };
      this.messages.push(data);
    });

    this.registerRoom();

    if (this.auth.loggedIn()) {
      this.newUser();
    } else if (!this.auth.loggedIn()) {
      this.newGuest();
    }

    //window.location.href='/profile';
  }
}

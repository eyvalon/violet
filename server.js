// ******************************************
// INITIALIZATION
// ******************************************
// Server specific version of Zone.js for SSR
require('zone.js/dist/zone-node');

// Local storage for angular universal
require('localstorage-polyfill');

// Dependencies
const express = require('express');
const ngUniversal = require('@nguniversal/express-engine');
// const cors = require('cors');

// Express and Port
const app = express();
const port = process.env.PORT || 4000;

// Server Bundle
// const appServer = require('./dist/server/main.bundle');
const { AppServerModuleNgFactory } = require('./dist/server/main.bundle');
// const angular = require('./routes/angular');

function angularRouter(req, res) {
    /* Server-side Rendering */
    res.render('index', {req, res});
}

// ******************************************
// MIDDLEWARE
// ******************************************
// Cross-Origin Resource Sharing (CORS)
// app.use(cors());

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// ******************************************
// ROUTES
// ******************************************

// Server side-rendering of root route
app.get('/', angularRouter);

// Serve static files
app.use(express.static(`${__dirname}/dist/browser`));

// Configure Angular Express engine
app.engine(
    'html', 
    ngUniversal.ngExpressEngine({
        bootstrap: AppServerModuleNgFactory
    })
);
app.set('view engine', 'html');
app.set('views', 'dist/browser');

// Direct all other routes to index.html
app.get('*', angularRouter);

// ******************************************
// API ERROR HANDLER
// ******************************************

// app.use(function (req, res, next) {
//     // Website you wish to allow to connect
//     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST');

//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);

//     // Pass to next layer of middleware
//     next();
// });

// Error handler for 404 - Page Not Found
// app.use(function (req, res, next) {
//     let err = new Error('Not Found');
//     res.status(404).json({
//         status: 404,
//         message: err.message,
//         name: err.name
//     });
// });

// // Error handler for all other errors
// app.use(function (err, req, res, next) {
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};
//     res.status(err.status || 500).json({
//         status: 500,
//         message: err.message,
//         name: err.name
//     });
// });

// Chat server to response to
const server = require('http').Server(app);

app.set('port', port);

// ******************************************
// SERVER START
// ******************************************
server.listen(port, () => console.log(`Server started on port ${port}`));

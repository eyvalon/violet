# Built with Angular Universal with integration of Real Time search build using states (Angular 5)

## Versions Used
* Express v4.16.2
* Angular v5.2.8
* Node.js v9.8.0

## Starting the Project
To begin working with this project, perform the following tasks:

1. Clone the repo
2. Make sure to set your backend service that hosts API to localhost with port 3000 (this can be done by using nginx):

location /api {
        proxy_pass http://localhost:3000;
}

3. Run `npm install` to download dependencies
4. Run `npm start` to generate the */dist* folder for deployment OR run `npm run serve` to start local development

## Description
This project is a self-learning Angular project to get used to services as well learning how Angular works. The approach that we took for this is that, we wanted to make a website that utilizes API and finding a way to use that for searching efficiently across components.

The API that we used for initialially grabbing to store in our databse is from: https://api.jikan.moe/v3/anime/. However, as this is only used for learning purposes, that information is then stored to our databases to ensure that we are using a centralised database without needing to rely on another provider hence reduce API requests.

## Things that have been implemented:

* Using states to store data and use that for searching (MongoDB)
* Using services to use as centralised communication
* Uses TypeScript instead of JavaScript to provide better coding with OOP
* Separated backend and frontend so that one can be used for hosting elsewhere instead of bundling into one whole project
* Lazy load image (compatibility with Angular 5 version)

## Things that have not been implemented:

* Fix up header navigations for better navigation and using proper icons
* Optimize for mobile experience

## Images
![Alt Text](./images/Showcase.gif)
